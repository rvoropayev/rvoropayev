﻿#Theory

Get-Process | Export-Csv C:\scripts\Module3\process.csv

Get-Process | Select Name,Path,Company | Export-Csv C:\scripts\Module3\FilteredProcess.csv -NoTypeInformation -Delimiter ';' -Encoding Unicode

Get-WmiObject Win32_logicaldisk -ComputerName LocalHost | Select `
@{Name="Drive Letter";Expression={($_.DeviceID)}},`
@{Name="Size(GB)";Expression={[decimal]("{0:N2}" -f($_.size/1gb))}}, `
@{Name="Free Space(GB)";Expression={[decimal]("{0:N2}" -f($_.freespace/1gb))}}, `
@{Name="Free (%)";Expression={"{0,6:P0}" -f(($_.freespace/1gb) / ($_.size/1gb))}} `
| Export-CSV C:\scripts\Module3\FreeDiskInformation.csv -NoTypeInformation

#Practic

Add-AzureRmAccount
Select-AzureRMSubscription -SubscriptionName 'Free Trial'

Get-AzureRmResource | Export-Csv C:\scripts\Module3\SubscriptionResources.csv -NoTypeInformation