﻿Login-AzureRmAccount

$automationAccountName = "Rost-AutoAcc-CI"
$RGName = "Rost-Azure-Training"
$TimeZone = ([System.TimeZoneInfo]::Local).Id
$StartTime1 = "9/17/2018 9:00:00"
$StartTime2 = "9/17/2018 19:00:00"

New-AzureRMAutomationSchedule –AutomationAccountName $automationAccountName –Name "StartVMs-Schedule" `
-StartTime $StartTime1 -DayInterval 1 -ResourceGroupName $RGName -TimeZone $TimeZone

New-AzureRMAutomationSchedule –AutomationAccountName $automationAccountName –Name "StopVMs-Schedule" `
-StartTime $StartTime2 -DayInterval 1 -ResourceGroupName $RGName -TimeZone $TimeZone

Register-AzureRmAutomationScheduledRunbook –AutomationAccountName $automationAccountName `
–Name "Start-all-VMs" –ScheduleName "StartVMs-Schedule" -ResourceGroupName $RGName

Register-AzureRmAutomationScheduledRunbook –AutomationAccountName $automationAccountName `
–Name "Stop-all-VMs" –ScheduleName "StopVMs-Schedule" -ResourceGroupName $RGName