﻿Login-AzureRMAccount
Select-AzureRMSubscription -SubscriptionName 'Free Trial'

$resourceGroup = "Rost-Azure-Test"
$location = "North Europe"

New-AzureRmResourceGroup -Name $resourceGroup -Location $location

$storageAccountName = "rostpsodiaas"
New-AzureRmStorageAccount -Name $storageAccountName -ResourceGroupName $resourceGroup `
                          -Type Standard_LRS -Location $location

$vnetName = "rost-iaas-net"
$subnet = New-AzureRmVirtualNetworkSubnetConfig -Name frontendSubnet -AddressPrefix 10.0.1.0/24
$vnet = New-AzureRmVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroup -Location $location `
                                  -AddressPrefix 10.0.0.0/16 -Subnet $subnet

$nicName = "rost-vm1-nic"
$pip = New-AzureRmPublicIpAddress -Name $nicName $ -ResourceGroupName $resourceGroup `
                                  -Location $location -AllocationMethod Dynamic
$nic = New-AzureRmNetworkInterface -Name $nicName -ResourceGroupName $resourceGroup `
                                   -Location $location -SubnetId $vnet.Subnets[0].Id -PublicIpAddressId $pip.Id

$vmName = "Rost-WinWM-1"
$vm = New-AzureRmVMConfig -VMName $vmName -VMSize Standard_DS1_v2

$cred = Get-Credential -Message "Admin credential"
$vm = Set-AzureRmVMOperatingSystem -VM $vm -Windows -ComputerName $vmName -Credential $cred `
                                   -ProvisionVMAgent -EnableAutoUpdate
$vm = Set-AzureRmVMSourceImage -VM $vm -PublisherName "MicrosoftWindowsServer" `
                               -Offer "WindowsServer" -Skus "2016-Datacenter" -Version latest

$vm = Add-AzureRmVMNetworkInterface -VM $vm -Id $nic.Id

$diskName = "os-disk"
$storageAcc = Get-AzureRmStorageAccount -ResourceGroupName $resourceGroup -Name $storageAccountName
$osDiskUri = $storageAcc.PrimaryEndpoints.Blob.ToString() + "vhds/" + $diskName + ".vhd"
$vm = Set-AzureRmVMOSDisk -VM $vm -Name $diskName -VhdUri $osDiskUri -CreateOption fromImage

New-AzureRmVM -ResourceGroupName $resourceGroup -Location $location -VM $vm

#End of Creating VM

$rg = Get-AzureRmResourceGroup -Name "Rost-Azure-Training"
$vm = Get-AzureRmVM -ResourceGroupName "Rost-Azure-Training" -Name "Rost-VM1"
$nic = Get-AzureRmNetworkInterface -ResourceGroupName $rg.ResourceGroupName -Name $(Split-Path -Leaf $VM.NetworkProfile.NetworkInterfaces[0].Id)
$nic | Get-AzureRmNetworkInterfaceIpConfig | Select-Object Name,PrivateIpAddress,@{'label'='PublicIpAddress';Expression={Set-Variable -name pip -scope Global -value $(Split-Path -leaf $_.PublicIpAddress.Id);$pip}}
(Get-AzureRmPublicIpAddress -ResourceGroupName $rg.ResourceGroupName -Name $pip).IpAddress

Get-AzureRmPublicIpAddress -ResourceGroupName "Rost-Azure-Training" -Name Rost-VM1-ip
