﻿Login-AzureRmAccount
Get-AzureRmSubscription -SubscriptionId b5c3c085-8789-4fb7-98d7-13af96df1709

### Define variables
$resourceGroupLocation = 'West Europe'
$resourceGroupName = 'rostTrainingIaas'
$resourceDeploymentName = 'rostTrainingIaas'
$templatePath = $env:SystemDrive + '\code\rvoropayev\module5'
$templateFile = 'azureDeployment.json'
$template = $templatePath + '\' + $templateFile
$password = 'SuperPass2000'
$securePassword = $password | ConvertTo-SecureString -AsPlainText -Force

### Create Resource Group
New-AzureRmResourceGroup `
    -Name $resourceGroupName `
    -Location $resourceGroupLocation `
    -Verbose -Force

### Deploy Resources
$additionalParameters = New-Object -TypeName Hashtable
$additionalParameters['vmAdminPassword'] = $securePassword

New-AzureRmResourceGroupDeployment `
    -Name $resourceDeploymentName `
    -ResourceGroupName $resourceGroupName `
    -TemplateFile $template `
    @additionalParameters `
    -Verbose -Force