﻿Login-AzureRmAccount

Configuration WebServerInstall {

    Import-DscResource -ModuleName PsDesiredStateConfiguration

    Node 'rost-vm01' {

        WindowsFeature WebServer {
            Ensure = "Present"
            Name   = "Web-Server"
        }

        File WebsiteContent {
            Ensure = 'Present'
            SourcePath = 'c:\temp\index.htm'
            DestinationPath = 'c:\inetpub\wwwroot'
        }
    }
}
WebServerInstall